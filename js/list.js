(function(){
    $.fn.extend({
        'ui.Page': function()
        {
            var self = this

            self.init = function()
            {
                var list = self.find('#list')['ui.List']()
                list.init()
                list.load()
            }

            return self
        }
    })

    $(document).ready(function(){
        var page = $('#container')['ui.Page']()
        page.init()
    })
})(jQuery)