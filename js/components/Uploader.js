/**
 * Created by quxiangqian on 2017/12/28.
 */

(function($){
    $.fn.extend({
        Uploader:function () {
            var self=this;
            self.init=function(){


                if ( !WebUploader.Uploader.support() ) {
                    alert( 'Web Uploader 部支持');
                    throw new Error( 'WebUploader does not support the browser you are using.' );
                }
                if(!$("body div[dialogs='true']").is("div")){
                    $("body").append('<div dialogs="true" class="dialogs"></div>')
                }
                var dialogs=$("body div[dialogs='true']");

                function drawUploadRect () {
                    if(!dialogs.find("#upload_rect").is("div"))
                    {
                        self.model=$('<div class="model"></div>')
                        self.dialogrect=$('<div class="dialogrect" ><div class="body">将文件拖动到当前位置上传 </div></div>');
                        self.dialog=$('<div class="dialog" id="upload_rect"></div>');
                        self.dialog.append(self.model);
                        self.dialog.append(self.dialogrect);
                        dialogs.append(self.dialog);
                    }
                }

                drawUploadRect ();
                var proccess=null;


                var uploader = WebUploader.create({
                    server: self.attr("server"),
                    pick: {id:self.get(0),innerHTML:'<i class="fa fa-cloud-upload"></i>上传'},
                    resize: false,
                    dnd:document.body,
                    auto: true,

                });
                document.body.addEventListener('dragover', function (e) {
                    e.stopPropagation();
                    e.preventDefault();
                    self.dialog.show();
                    var level=Date.parse(new Date())/1000;
                    var _left=($(window).width()-self.dialogrect.outerWidth())/2;
                    var _top=($(window).height()-self.dialogrect.outerHeight())/2;
                    self.dialogrect.css({left:_left,top:_top})
                    self.dialog.css('z-index',level)
                }, false);

                document.body.addEventListener('drop', function (e) {
                    e.stopPropagation();
                    e.preventDefault();
                    self.dialog.hide();
                }, false);

                uploader.on('uploadProgress', function( file, percentage ) {
                        proccess.setProccess("上传进度（"+percentage+"／100%）",percentage);
                }
                );
                uploader.on('uploadError',function (file,reason) {
                    //console.log("上传文件错误",reason)
                    $.Tost("上传文件错误").show().autoHide(2000);
                });
                uploader.on( 'error', function(type) {
                    console.log(type)

                }
                );


                uploader.on( 'uploadBeforeSend', function(obj,data,headers) {
                        console.log("BeforeSend",obj,data,headers);
                    }
                );
                uploader.on( 'uploadSuccess', function(file,response) {
                    $.Tost("上传文件成功").show().autoHide(2000);
                    }
                );
                uploader.on( 'uploadComplete', function(file) {
                      proccess.hide();
                    }
                );


                uploader.on('fileQueued', function( file ) {
                    proccess=$.Proccess();
                    proccess.show();
                    //uploader.upload(file);

                });
            }
            return self;
        }
    })
})(jQuery)